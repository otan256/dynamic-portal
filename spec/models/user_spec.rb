require 'spec_helper'

describe User do

  before(:each) do
    @user = User.new
  end

  let(:user) { FactoryGirl.build(:user) }

  #context 'associations' do
  #  it { should have_many(:posts)}
  #  it { should have_many(:messages)}
  #end



  #describe 'validations' do
  #  it {@user.should validate_presence_of :email, :first_name}
  #  it {@user.should validate_uniqueness_of :email}
  #end

  it {@user.active.should == false }

  it 'should have token' do
    @user.generate_token(:remember_me_token)
    @user.remember_me_token.should_not be_nil
  end

  context 'facebook integration' do
    email = 'test@mail.one'
    pass = 'test_pass'
    @user = User.new({provider: 'facebook', active: true,
                     email: email, password: pass})
    @user.from_facebook?.should == true
    @user.active.should == true

  end
end
