source 'https://rubygems.org'

ruby '1.9.3' # for heroku
# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 3.2.8'

# Use mysql as the database for Active Record
gem 'mysql2'
gem 'activerecord-postgresql-adapter'

group :assets do

# Use SCSS for stylesheets
  gem 'sass-rails'

# Use CoffeeScript for .js.coffee assets and views
  gem 'coffee-rails'

# gem 'therubyracer', platforms: :ruby
  gem 'uglifier'

# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
  gem 'turbolinks'

  gem 'twitter-bootstrap-rails'
end

# Use jquery as the JavaScript library
gem 'jquery-rails'
gem 'haml'
gem 'haml-rails'
gem 'less-rails'
gem 'less-rails-bootstrap'
gem 'therubyracer'

group :doc do
  # bundle exec rake doc:rails generates the API under doc/api.
  gem 'sdoc', require: false
end

group :development, :test do
  gem 'rspec-rails'
  gem 'guard-rspec'
  gem 'factory_girl_rails'
  gem 'turn'
end

group :production do
  gem 'bundler'
  gem 'whenever'
  gem 'newrelic-redis'
  gem 'rpm_contrib'
  gem 'newrelic_rpm', '~> 3.5.3'
  gem 'exception_notification', :git => 'git://github.com/rails/exception_notification', :require => 'exception_notifier'
end

group :development do
  gem 'capistrano'
  gem 'better_errors'
  gem 'binding_of_caller'
  gem 'brakeman'
  gem 'bullet'
end

gem 'thin'

# For Pagination
gem 'will_paginate'

# NOSQL database
gem 'redis'

# Cron Jobs
gem 'whenever', :require => false

# To authorize with facebook
gem 'omniauth-facebook'
# To authorize with twitter
gem 'omniauth-twitter'
# To authorize with vk
gem 'omniauth-vkontakte'

# To send sms
gem 'nexmo'

gem 'simple_form'
gem 'country_select'

gem 'recaptcha', :require => 'recaptcha/rails'
