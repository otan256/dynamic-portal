class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.integer :author_id
      t.text :content
      t.string :title
      t.integer :likes, default: 0

      t.timestamps
    end
  end
end
