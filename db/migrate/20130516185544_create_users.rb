class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :first_name
      t.string :last_name
      t.string :password
      t.string :email
      t.string :phone
      t.boolean :active, :default => false
      t.string :avatar, :default => 'no_avatar.jpg'
      t.string :settings
      t.datetime :birth_date
      t.string :country
      t.text :about
      t.string :ip_address
      t.string :last_ip
      t.string :remember_me_token
      t.string :password_reset_token
      t.datetime :password_reset_sent_at
      t.datetime :last_seen_at
      # omniauth integration:
      t.string :provider
      t.bigint :uid
      t.string :oauth_token
      t.datetime :oauth_expires_at

      t.timestamps
    end
  end
end
