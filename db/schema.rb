# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130612003130) do

  create_table "friendships", :force => true do |t|
    t.integer "user_id"
    t.integer "friend_id"
  end

  create_table "messages", :force => true do |t|
    t.text     "content",     :limit => 16777215
    t.integer  "author_id"
    t.integer  "receiver_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "posts", :force => true do |t|
    t.integer  "author_id"
    t.text     "content",    :limit => 16777215
    t.string   "title"
    t.integer  "likes",                          :default => 0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", :force => true do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "password"
    t.string   "email"
    t.string   "phone"
    t.string   "settings"
    t.datetime "birth_date"
    t.string   "country"
    t.text     "about",                  :limit => 16777215
    t.string   "ip_address"
    t.string   "last_ip"
    t.string   "remember_me_token"
    t.string   "password_reset_token"
    t.datetime "password_reset_sent_at"
    t.datetime "last_seen_at"
    t.string   "provider"
    t.string   "oauth_token"
    t.datetime "oauth_expires_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "active",                                     :default => false
    t.string   "avatar",                                     :default => "no_avatar.jpg"
    t.integer  "uid",                    :limit => 8
  end

  add_index "users", ["email"], :name => "index_users_on_email"
  add_index "users", ["first_name"], :name => "index_users_on_first_name"
  add_index "users", ["ip_address"], :name => "index_users_on_ip_address"
  add_index "users", ["last_name"], :name => "index_users_on_last_name"

end
