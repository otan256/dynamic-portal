DynamicPortal::Application.routes.draw do

  root to: 'welcome#index'

  match 'auth/:provider/callback', to: 'integrations#create', via: [:get]
  match 'auth/failure', to: redirect('/'), via: [:get]

  match '/portal' => 'portal#index', :as => :portal_index, via: [:get]
  match '/set_localization/(:locale)' => 'portal#set_localization', as: :set_localization, via: [:post]
  match '/logout' => 'portal#logout', :as => :logout, via: [:get]

  resources :password_restore

  resources :welcome do
    collection do
      get :about
      post :do_login
      get :forgot
      get 'activate/(:token)' => 'welcome#activate', as: :activate
    end
  end

  resources :api do
    collection do
      get :get_user, :defaults => { :format => 'json' }
    end
  end

  namespace :portal do

    match 'dashboard' => 'dashboard#index', as: :dashboard_index, via: [:get]
    namespace :dashboard do

      resources :posts do
        resources :comments
      end

      resources :messages

      resources :events
    end

    resources :friendships do
      collection do
        post 'create/(:friend_id)' => 'friendships#create', as: :create
      end
    end

    resources :users

    resources :games

  end
end
