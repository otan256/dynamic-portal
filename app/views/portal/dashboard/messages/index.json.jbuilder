json.array!(@messages) do |message|
  json.extract! message, :content, :author_id, :receiver_id
  json.url message_url(message, format: :json)
end