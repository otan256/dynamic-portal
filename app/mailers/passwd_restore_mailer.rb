class PasswdRestoreMailer < ActionMailer::Base
  default from: "dynamicPortal@support.com"

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.user_mailer.password_reset.subject
  #
  def password_restore(user)
    @user = user
    mail to: user.email, subject: t('welcome.registration.password_restore.subject')
  end
end
