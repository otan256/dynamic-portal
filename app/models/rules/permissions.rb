module Rules
  module Permissions

    def can_edit?(activity)
      return false if self.id != activity.id

      true
    end

  end
end