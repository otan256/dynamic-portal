module Rules
  module UserRules

    def can_register_user?(attr)
      return false unless valid_password?(attr[:password_digest])
      return false unless valid_credentials?(attr[:name], attr[:surname])
      true
    end

    def valid_credentials?(first_name, last_name)
    #TODO make it more serious
      if first_name.blank? || last_name.blank?
        return false
      end

      true
    end

    def valid_password?(password)

      if password.blank? || password.size < 4
        return false
      end

      true
    end

  end
end