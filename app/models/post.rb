class Post < ActiveRecord::Base

  belongs_to :user, foreign_key: :author_id
  #has_many :answers
  attr_accessible :author_id, :title, :content

  validates_presence_of :title, :content, :author_id

  scope :newly_added, where('created_at >= ?', Time.now - 1.week)
  # TODO fix this
  scope :unanswered, where(author_id: 1)
  scope :my, lambda { |id| where('author_id = ?', id) }
  scope :hot, where('likes >= ?', 10 )

  def author
    User.find_by_id self.author_id
  end

end
