# coding: utf-8

# md5 is needed for pasword generation
require 'digest/md5'

class User < ActiveRecord::Base

  attr_accessible :email, :first_name, :last_name, :password, :updated_at

  serialize :settings, Hash

  include FileUtils
  include UserExt::Settings
  include UserExt::Registration
  include Rules::UserRules
  include Rules::Permissions
  include RedisSupport::RedisHelper

  has_many :posts
  has_many :messages
  has_many :friendships
  has_many :friends, :through => :friendships
  has_many :inverse_friendships, class_name: 'Friendship', foreign_key: :friend_id
  has_many :inverse_friends, :through => :inverse_friendships, :source => :user

  validates_confirmation_of :password
  validates_presence_of :first_name, :email, :password
  validates_uniqueness_of :email

  before_create { generate_token(:remember_me_token) }

  class << self
    include UserExt::SocialIntegration
  end

  def active?
    self.active
  end

  def is_author?(activity)
    activity.author_id == self.id
  end

  def followed?(user)
    query = user.class == String || user.class == Integer ? user : user.id
    !self.friendships.where(friend_id: query).blank?
  end

  def generate_token(column)
    begin
      self[column] = SecureRandom.urlsafe_base64
    end while User.exists?(column => self[column])
  end

  def online?
    if cache_get(compose_key)
      'online' if cache_get(compose_key).to_time > 10.minutes.ago
    end
  end

  def update_online
    cache_put compose_key, Time.now.to_s
  end

  def reset_password!(attr)
    self.password = User.encrypt_a_password(attr[:password]) if attr[:password]
    self.save!
  end


  def authenticate(user_mail, user_password)
    encrypted_password = User.encrypt_a_password(user_password)
    r = User.all :conditions => ['email = ? and password = ?', user_mail, encrypted_password]

    unless r
      encrypted_password = User.encrypt_a_password(user_password, true)
      r = User.all :conditions => ['email = ? and password = ?', user_mail, encrypted_password]
    end

    r
  end

  def from_social?
    self.provider == 'facebook' || self.provider == 'twitter' || self.provider == 'vkontakte'
  end

  def update_edited(attr)
    self.first_name = attr[:first_name]
    self.last_name = attr[:last_name]
    self.phone = attr[:phone]
    self.about = attr[:about]
    self.country = attr[:country]
    self.birth_date = Date.parse attr[:birth_date] unless attr[:birth_date].blank?
    update_avatar!(attr[:avatar]) if attr[:avatar]
    self.save!
  end

  def update_avatar!(avat)
    name = "#{self.id.to_s}.jpg"
    File.open(Rails.root.join('public', 'tmp_avatars', name), 'wb') do |file|
      file.write(avat.read)
    end

    FileUtils.cp "public/tmp_avatars/#{name}", "#{Rails.root}/app/assets/images/avatars/#{name}"
    File.delete("public/tmp_avatars/#{name}") if File.exist?("public/tmp_avatars/#{name}")
    self.avatar = name
  end

  def self.online_users
    self.all.select {|user| user if user.online?}
  end

  private

  # private class methods
  def self.encrypt_a_password(password, case_sensitive = false)
    password = password.mb_chars.downcase unless case_sensitive
    Digest::MD5.hexdigest(password)
  end


end
