module UserExt
  module Registration

    def send_account_activation
      RegistrationMailer.account_activation(self).deliver
    end

    def send_password_reset
      generate_token(:password_reset_token)
      self.password_reset_sent_at = Time.zone.now
      save!
      PasswdRestoreMailer.password_restore(self).deliver
    end

    def activate_account!
      self.active = true
      self.save!
    end
  end
end