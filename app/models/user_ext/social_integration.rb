# coding: utf-8

module UserExt
  module SocialIntegration

    def registered_with_social?(auth)
      self.find_by_uid(auth.uid.to_i)
    end

    def from_omniauth(auth)
      if self.registered_with_social?(auth)
        user = find_by_email(auth.info.email) || find_by_uid(auth.uid)
        user.uid = auth.uid
        user.oauth_token = auth.credentials.token
        user.oauth_expires_at = Time.at(auth.credentials.expires_at) if auth.credentials.expires_at
        user.save!
        return user
      else
        where(auth.slice(:provider, :uid, :email)).first_or_initialize.tap do |user|
          user.provider = auth.provider
          user.uid = auth.uid.to_i
          if auth.provider == 'twitter'
            user.first_name = auth.info.nickname
          else
            user.first_name = auth.info.first_name
            user.last_name = auth.info.last_name
          end
          user.oauth_token = auth.credentials.token
          user.avatar = auth.info.image
          user.password = generate_value
          user.country = auth.info.location
          user.settings = auth.info.urls
          user.email =  auth.info.email ? auth.info.email : generate_value
          user.oauth_expires_at = Time.at(auth.credentials.expires_at) if auth.credentials.expires_at
          user.save!
          return user
        end
      end
      nil
    end

    def generate_value
       SecureRandom.uuid
    end

  end
end