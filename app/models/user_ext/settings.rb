module UserExt::Settings

  def get_locale
    self.settings[:locale] || I18n.default_locale
  end

  def set_locale(locale)
    unless locale.blank?
      self.settings.merge!({locale: locale})
      self.save!
    end
  end

end