require 'integration_helper'

module ApplicationHelper

  include RedisSupport::RedisHelper

  def to_human(date)
    date.strftime('%d/%m/%Y')
  end

  #def icon_link_to(path, opts = {}, link_opts = {})
  #  classes = []
  #  [:icon, :blank].each do |klass|
  #    k = opts.delete(klass)
  #      classes << "#{klass}-#{k}" if k
  #  end
  #  classes << 'enlarge' if opts.delete(:enlarge)
  #  opts[:class] ||= ''
  #  opts[:class] << ' ' << classes.join(' ')
  #  link_to content_tag(:i, '', opts), path, link_opts
  #end

  def copyright_notice_year_range(start_year)
    # In case the input was not a number (nil.to_i will return a 0)
    start_year = start_year.to_i

    # Get the current year from the system
    current_year = Time.new.year
    # When the current year is more recent than the start year, return a string
    # of a range (e.g., 2010 - 2012). Alternatively, as long as the start year
    # is reasonable, return it as a string. Otherwise, return the current year
    # from the system.
    if current_year > start_year && start_year > 2000
      "#{start_year} - #{current_year}"
    elsif start_year > 2000
      "#{start_year}"
    else
      "#{current_year}"
    end
  end

  def online_count
    redis_count = cache_get(:users_online_count.to_s)
    last_updated_redis_count = cache_get(:last_updated_users_online_count.to_s)
    if redis_count && last_updated_redis_count && last_updated_redis_count.to_time < 10.minutes.ago
      redis_count
    else
      sql_count = User.count conditions: ['updated_at > ?', Time.now.utc - 10.minutes]
      cache_put :users_online_count.to_s, sql_count
      cache_put :last_updated_redis_count.to_s, Time.now.to_s
      sql_count
    end
  end

  def in_social_box?
    in_facebook_box? || in_vk_box?
  end

  def in_facebook_box?
    URI.parse(current_url).host == IntegrationHelper::FACEBOOK_URL
  end

  def in_vk_box?
    URI.parse(current_url).host == IntegrationHelper::VK_URL
  end

end
