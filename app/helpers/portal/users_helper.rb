module Portal::UsersHelper

  def avatar_tag(user, options = {})
    size = options[:small] ? '70x70' : '200x200'
    size = '30x30' if options[:tiny]
    clazz = options[:rounded] ? 'img-circle' : 'img-polaroid'

    if user.from_social?
      link_to image_tag(user.avatar, size: size, class: clazz), portal_user_path(user.id)
    else
      link_to image_tag("avatars/#{user.avatar}", size: size, class: clazz), portal_user_path(user.id)
    end
  end

  def username(user)
    if user.last_name
      link_to user.first_name + ' ' + user.last_name, portal_user_path(user.id)
    else
      link_to user.first_name, portal_user_path(user.id)
    end
  end

  def info_tag(parameter)
    parameter ? content_tag(:div, parameter, class: 'user_info') : t('globals.empty')
  end

  def email_tag(email)
    email =~ /^.+@.+$/ ? email : 'Email: ' + t('globals.empty')
  end

end
