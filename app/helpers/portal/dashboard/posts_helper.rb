module Portal::Dashboard::PostsHelper

  def posted_at(post)
    'created ' + time_ago_in_words(post.created_at) + ' ago'
  end

end
