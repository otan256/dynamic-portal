module IntegrationHelper

  FACEBOOK_URL = 'apps.facebook.com'
  VK_URL = 'vk.com'

  LOGIN_WITH_FACEBOOK_URL = '/auth/facebook'
  LOGIN_WITH_TWITTER_URL = '/auth/twitter'
  LOGIN_WITH_VK_URL = '/auth/vkontakte'

  NOT_IN_SOCIAL_BOX_ERROR = "NOT IN SOCIAL BOX CAN'T AUTOLOGIN!!! FIX ME IMMEDIATELY!!!"

end