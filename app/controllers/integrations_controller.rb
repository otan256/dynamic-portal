class IntegrationsController < ApplicationController

  def create
    user = User.from_omniauth(env['omniauth.auth'])
    if user
      cookies[:remember_me_token] = user.remember_me_token
      redirect_to portal_games_path
    else
      redirect_to_with_notice root_url, t('notices.login.facebook_failed'), :error
    end
  end

end