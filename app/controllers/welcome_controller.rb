class WelcomeController < ApplicationController

  include LoginExt
  include ApplicationHelper

  def index
    @source = params[:source] if params[:source]
    make_auto_log_in if in_social_box?
    redirect_if_logged_in
    @user = User.new
  end

  def activate
    @user = User.find_by_remember_me_token params[:token] if params[:token]
    if @user && !@user.active?
      @user.activate_account!
      redirect_to_with_notice root_path, t('notices.registration.activation_success'), :success
    elsif @user && @user.active?
      redirect_to_with_notice root_path, t('notices.registration.account_activated')
    end
    redirect_to_with_notice root_path, t('notices.registration.account_activation_failed'), :error
  end

end
