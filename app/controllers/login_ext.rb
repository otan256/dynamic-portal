module LoginExt

  def do_login

    source1 = params[:source]
    email = params[:email]
    password = params[:password]
    url = source1 == 'chess' ? get_user_api_index_path : portal_games_path

    unless email.blank? || password.blank?
      users_array = User.find_by_email(email).try(:authenticate, email, password)
      if users_array && !users_array.blank?
        redirect_to_with_notice url, t('notices.login.success', username: users_array.first.first_name) if login_user(users_array.first)
      end
    end
    redirect_to_with_notice root_path, t('notices.login.invalid_login_or_password'), :error
  end

  def make_auto_log_in
    redirect_to IntegrationHelper::LOGIN_WITH_FACEBOOK_URL if in_facebook_box?
    redirect_to IntegrationHelper::LOGIN_WITH_VK_URL if in_vk_box?
  end

  private

  def login_user(user)
    return false unless user.active?

    if params[:remember_me]
      cookies.permanent[:remember_me_token] = user.remember_me_token
    else
      cookies[:remember_me_token] = user.remember_me_token
    end
    true
  end

end