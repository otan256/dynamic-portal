class PortalController < ApplicationController

  before_filter :authorize_access, :except => [:logout, :create]
  #after_filter :update_online

  def index
    redirect_to(portal_user_path(current_user.id))
  end

  def authorize_access
    unless logged_in?
      redirect_to_with_notice root_path, t('notices.login.authorization_required'), :error
    end
  end

end
