class Portal::FriendshipsController < PortalController
  def create
    @friendship = current_user.friendships.build(:friend_id => params[:friend_id])
    friend = User.find_by_id params[:friend_id]
    ###
    # not needed as post is not bookmarkable
    # redirect_to_with_notice portal_users_path, t('users.friendships.already_added') if current_user.followed? params[:friend_id]
    ###
    if @friendship.save
      redirect_to_with_notice portal_users_path, t('users.friendships.friend_added', name: friend.first_name )
    else
      redirect_to_with_notice portal_user_path(params[:friend_id]), t('users.friendships.cannot_add')
    end
  end

  def destroy
    @friendship = current_user.friendships.find(params[:id])
    @friendship.destroy
    redirect_to_with_notice portal_user_path(current_user.id), t('users.friendships.friend_removed', name: friend.first_name )
  end
end