class Portal::DashboardController < PortalController

  def index
    type = params[:what]
    @post = Post.new
    @posts = Post.newly_added if type == 'new'
    @posts = Post.unanswered if type == 'unanswered'
    @posts = Post.hot if type == 'hot'
    @posts = Post.my(current_user.id) if type == 'my'

    @posts ||= Post.all
  end

end
