class Portal::UsersController < PortalController
  before_filter :set_user, only: [:show, :edit, :update]

  # GET /users
  # GET /users.json
  def index
    @users = User.online_users if params[:list] == 'online'
    @users = current_user.friends if params[:list] == 'friends'
    @users ||= User.all
  end

  # GET /users/1
  # GET /users/1.json
  def show
  end

  # GET /users/new
  def new
    @user = User.new
  end

  # GET /users/1/edit
  def edit
    redirect_to_with_notice portal_users_path, t('notices.users.cannot_edit'), :error unless current_user.can_edit?(@user)
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(prepared_params_to_register)

    respond_to do |format|
      #redirect_to_with_notice root_path, t('notices.registration.failed_captcha', :error) unless verify_recaptcha(model: @user)
      if @user.save
        @user.send_account_activation
        format.html { redirect_to_with_notice root_path, t('notices.registration.success'), :success }
        format.json { render action: 'show', status: :created, location: @user }
      else
        format.html { redirect_to_with_notice root_path, t('notices.registration.failed', :error) }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    respond_to do |format|
      if @user.update_edited(prepared_params_to_update)
        format.html { redirect_to portal_user_path(@user), notice: t('notices.users.update_prof') }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    def prepared_params_to_register
      redirect_to_with_notice root_path, t('notices.login.password_confirm'), :error unless params[:user][:confirm_password] == params[:user][:password]
      params[:user].except!(:confirm_password).merge!({password: User.encrypt_a_password(params[:user][:password])})
    end

    def prepared_params_to_update
      params[:user].merge!({country: params[:user][:country],
                         phone: params[:user][:phone],
                         avatar: params[:user][:avatar],
                         birth_date: params[:user][:birth_date],
                         about: params[:user][:about]})
    end
end
