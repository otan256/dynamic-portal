class Portal::Dashboard::PostsController < PortalController
  before_filter :set_post, only: [:show, :destroy]

  # GET /posts
  # GET /posts.json
  def index
    @posts = Post.all
  end

  # GET /posts/1
  # GET /posts/1.json
  def show
  end

  # GET /posts/new
  def new
    @post = Post.new
  end

  # POST /posts
  # POST /posts.json
  def create
    @post = Post.new(params[:post].merge!({author_id: current_user.id}))

    respond_to do |format|
      if @post.save
        format.html { redirect_to_with_notice portal_dashboard_index_path(@post), t('notices.dashboard.news.post_created'), :succeess }
        format.json { render action: 'show', status: :created, location: @post }
      else
        format.html { redirect_to_with_notice portal_dashboard_index_path(tab: 'news'), t('globals.error') }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /posts/1
  # DELETE /posts/1.json
  def destroy
    @post.destroy
    respond_to do |format|
      format.html { redirect_to_with_notice portal_dashboard_index_path, t('notices.dashboard.news.post_deleted') }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_post
      @post = Post.find(params[:id])
    end

end
